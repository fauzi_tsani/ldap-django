import ldap
from django.contrib.auth.models import User, Group

AUTH_LDAP_SERVER = '127.0.0.1'
AUTH_LDAP_BASE_USER = "cn=admin,dc=fauzi,dc=com"
AUTH_LDAP_BASE_PASS = "admin"
AUTH_LDAP_BASE = "dc=fauzi,dc=com"
AUTH_LDAP_SCOPE = ldap.SCOPE_SUBTREE

def get_ldap_groups():
	scope = AUTH_LDAP_SCOPE
	filter = "(&(objectclass=posixGroup))"
	values = ['cn', 'memberUid']
	l = ldap.open(AUTH_LDAP_SERVER)
	l.protocol_version = ldap.VERSION3
	l.simple_bind_s(AUTH_LDAP_BASE_USER,AUTH_LDAP_BASE_PASS)
	result_id = l.search('ou=Groups,'+AUTH_LDAP_BASE, scope, filter, values)
	result_type, result_data = l.result(result_id, 1)
	l.unbind()
	return result_data
	
def sync_groups():
	messages = []
	ldap_groups = get_ldap_groups()
	for ldap_group in ldap_groups:
		try: group_name = ldap_group[1]['cn'][0]
		except: pass
		else:
			try: group = Group.objects.get(name=group_name)
			except Group.DoesNotExist:
				group = Group(name=group_name)
				group.save()
				message = "Group '%s' created." % group_name
				messages.append(message)
	message = "Groups are synchronized."
	messages.append(message)
	return messages

def get_ldap_users():
	scope = AUTH_LDAP_SCOPE
	filter = "uid=fauzi"
	#values = ['uid', 'mail', 'givenName', 'sn', ]
	values = []
	l = ldap.open(AUTH_LDAP_SERVER)
	l.protocol_version = ldap.VERSION3
	l.simple_bind_s(AUTH_LDAP_BASE_USER,AUTH_LDAP_BASE_PASS)
	result_id = l.search("dc=fauzi,dc=com", scope, filter, values)#'''+ AUTH_LDAP_BASE'''
	result_type, result_data = l.result(result_id, 1)
	l.unbind()
	return result_data
	
def sync_users():
	messages = sync_groups()
	ldap_users = get_ldap_users()
	ldap_groups = get_ldap_groups()
	for ldap_user in ldap_users:
		try: username = ldap_user[1]['uid'][0]
		except: pass
		else:
			try: email = ldap_user[1]['mail'][0]
			except: email = ''
			try: first_name = ldap_user[1]['givenName'][0]
			except: first_name = username
			try: last_name = ldap_user[1]['sn'][0]
			except: last_name = ''
			try: user = User.objects.get(username=username)
			except User.DoesNotExist:
				user = User.objects.create_user(username, email, username)
				user.first_name = first_name
				user.last_name = last_name
				message = "User '%s' created." % username
				messages.append(message)
			else:
				if not user.email == email:
					user.email = email
					message = "User '%s' email updated." % username
					messages.append(message)
				if not user.first_name == first_name:
					user.first_name = first_name
					message = "User '%s' first name updated." % username
					messages.append(message)
				if not user.last_name == last_name:
					user.last_name = last_name
					message = "User '%s' last name updated." % username
					messages.append(message)
			user.save()
			for ldap_group in ldap_groups:
				group_name = ldap_group[1]['cn'][0]
				group_members =	ldap_group[1]['memberUid']
				try:
					group = Group.objects.get(name=group_name)
				except:
					pass
				else:
					if not user.username in group_members:
						if group in user.groups.all():
							user.groups.remove(group)
							message = "User '%s' removed from group '%s'." % (user.username, group.name)
							messages.append(message)
					else:
						if not group in user.groups.all():
							user.groups.add(group)
							message = "User '%s' added to group '%s'." % (user.username, group.name)
							messages.append(message)
	message = "Users are synchronized."
	messages.append(message)
	return messages
