#library LDAP
import ldap
import ldap.modlist as modlist

from django.contrib.auth.models import User, Group

#library return html
from django.http import HttpResponse
from django.template import RequestContext, Context, loader
from django.shortcuts import render_to_response

#library django_auth_ldap
from django_auth_ldap.backend import LDAPBackend,_LDAPUser

from syncldap import Command
#django-ldap-syncldap_sync.management.commands

from LDAPSync import *

def index(request):
    ask="run"
    return HttpResponse(ask)

def test(request):
    dn = 'cn=admin,dc=fauzi,dc=com'
    conn = ldap.initialize('ldap://127.0.0.1:389')
    conn.simple_bind_s('dc=fauzi,dc=com','admin')
    #filter = '(objectClass=person)'
    attrs = ['sn', 'givenName', 'uid']        
    raw = self.conn.search_s(dn, ldap.SCOPE_ONELEVEL, attrs)
    return HttpResponse(raw)

'''Masih perbaikan, menambah secara dynamic'''
def add(request): 
	
	if request.method == 'POST':
		l = ldap.initialize("ldap:///127.0.0.") #buka koneksi ldap
		l.simple_bind_s("cn=admin,dc=fauzi,dc=com","admin") #autentikasi dan terkoneksi dengan ldap

		dn="cn=replica,dc=fauzi,dc=com" #dn yang di tambahkan

		#attribut yang akan di tambahkan
		attrs = {}
		attrs['objectclass'] = ['top','organizationalRole','simpleSecurityObject']
		attrs['cn'] = 'replica'
		attrs['userPassword'] = 'admin'
		attrs['description'] = 'User object for replication using slurpd'

		# Convert
		ldif = modlist.addModlist(attrs)

		# sync dengan ldap
		l.add_s(dn,ldif)

		# disconnect dari ldap
		l.unbind_s()
	return render_to_response('postform.html', variables)

def delete(request):
	#binding ke ldap server
	try:
		l = ldap.open("127.0.0.1")
		l.protocol_version = ldap.VERSION2
		username = "cn=admin,dc=fauzi,dc=com"
		password  = "admin"
		l.simple_bind(username, password)
	except ldap.LDAPError, e:
		print e
		print "fail"

	#dn yang akan di delete
	deleteDN = "cn=replica,dc=fauzi,dc=com"
	try:
		l.delete_s(deleteDN)
	except ldap.LDAPError, e:
		print e
		print "failed"

def modify(request):
	# buka koneksi ldap
	l = ldap.initialize("ldap:///127.0.0.1")
	# bind ke server ldap
	l.simple_bind_s("cn=admin,dc=fauzi,dc=com","admin")
	
	#dn yang akan dirubah,*yang sudah ada di ldap
	dn="cn=replica,dc=fauzi,dc=com" 

	# variable untuk modify
	old = {'description':'Bind object used for replication using slurpd'}
	new = {'description':'User object for replication using slurpd'}
	#convert variable
	ldif = modlist.modifyModlist(old,new)
	#melakukan modify
	l.modify_s(dn,ldif)

	#disconnect dari server ldap
	l.unbind_s()

def search(request):
	try:
		l = ldap.open("127.0.0.1")
		l.protocol_version = ldap.VERSION2	
	except ldap.LDAPError, e:
		print e
		print "fail"

	#dn untuk mencari 
	baseDN = "dc=fauzi,dc=com"
	searchScope = ldap.SCOPE_SUBTREE
	## retrieve all attributes sesuai keinginan - baca dokumentasi lebih lanjut
	retrieveAttributes = None 
	#filter
	searchFilter = "uid=fauzi"
	try:
		ldap_result_id = l.search(baseDN, searchScope, searchFilter, retrieveAttributes)
		result_set = []
		while 1:
			result_type, result_data = l.result(ldap_result_id, 0)
			if (result_data == []):
				break
			else:
				if result_type == ldap.RES_SEARCH_ENTRY:
					result_set.append(result_data)
		#print result_set
		#for a in result_set:
		#	print a
		dn1 = str(result_set).replace("{","").replace("}","").replace("[","").replace("]","").replace("(","").replace(")","")
		dn0 = str(result_set).split(', ',1)
		dn = dn0[0].split("[[(")

		dncontent = dn0[1].split(', ')

		variables =  RequestContext(request, {'result':result_set,'dn':dn,'dncontent':dncontent,'dn1':dn1})
	

	except ldap.LDAPError, e:
		print e
		print "failed"	
		variables = "nullnull"
	return render_to_response('add.html', variables)

def custom(request):
	'''
	a = Command.get_ldap_users
	b = Command.sync_ldap_users
	print a
	print b
	return HttpResponse(str(a)+str(b))
	'''
	
	return HttpResponse(get_ldap_users())

